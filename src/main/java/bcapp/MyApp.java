/*
 * ====================================================================
 * Banno Boot Camp Application Assignment
 *
 * Morgen Ohmstede
 * February 25, 2018
 *
 * ====================================================================
 */
package bootcampapp;


import java.util.*;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MyApp {

  public static void main(String[] args) throws Exception {
    CloseableHttpClient httpclient = HttpClients.createDefault();
    try {
      HttpGet httpGet = new HttpGet("http://banno.com");
      CloseableHttpResponse response1 = httpclient.execute(httpGet);

      try {
        HttpEntity entity1 = response1.getEntity();
        // Convert response body to string for easier manipulation
        String htmlContent = EntityUtils.toString(entity1);

        Document dom = Jsoup.parse(htmlContent);

        // The top 3 occuring alphanumeric characters contained in the HTML, and how many times each occurs.
        top3AlphaNum(htmlContent);
        System.out.println("\n");

        // The number of .png images in the HTML.
        Elements pngImages = dom.select("img[src$=.png]");
        int numPngImages = pngImages.size();
        System.out.println("The number of PNG images in the HTML: " + numPngImages);

        // BannoJHA's Twitter handle: this should work if the Twitter name were to change.
        Elements twitterElement = dom.select("meta[name=twitter:site]");
        String twitterHandle = twitterElement.attr("content");
        System.out.println("BannoJHA's Twitter handle: " + twitterHandle);

        // The number of times the term "financial institution" occurs in text.
        String term = "financial institution";
        int subStrCount = (htmlContent.length() - htmlContent.replace(term, "").length()) / term.length();
        System.out.println("Number of times the term \"financial institution\" occurs in text: " + subStrCount);

        EntityUtils.consume(entity1);
      }
      finally {response1.close();}

      httpGet = new HttpGet("https://banno.com/features");
      CloseableHttpResponse response2 = httpclient.execute(httpGet);

      try
      {
        HttpEntity entity2 = response2.getEntity();
        String htmlContent = EntityUtils.toString(entity2);
        Document dom = Jsoup.parse(htmlContent);

        // A count of the number of products offered from https://banno.com/features.
        Elements products = dom.getElementsByClass("flex-item feature-group-label");
        int numOfProducts = products.size();
        System.out.println("The number of products offered from https://banno.com/features: " + numOfProducts);

        EntityUtils.consume(entity2);
      }
      finally {response2.close();}
    }
    finally {httpclient.close();}
  }

  public static void top3AlphaNum(String htmlContent)
  {
    Map<Character, Integer> htmlMap = new HashMap<>();
    for (int i = 0; i < htmlContent.length(); i++)
    {
      char ch = htmlContent.charAt(i);
      if (!Character.isLetterOrDigit(ch))
        continue;
      if (htmlMap.containsKey(ch))
        htmlMap.put(ch, htmlMap.get(ch) + 1);
      else
        htmlMap.put(ch, 1);
    }
    List list = new LinkedList(htmlMap.entrySet());
    Collections.sort(list, new Comparator()
    {
      public int compare(Object o1, Object o2) {
        return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
      }
    });
    // HashMap top3CharMap = new LinkedHashMap();
    Iterator it = list.iterator();
    for (int i = 0; i < 3; i++)
    {
      Map.Entry entry = (Map.Entry) it.next();
      // top3CharMap.put(entry.getKey(), entry.getValue());
      System.out.println("The character \"" + entry.getKey() + "\" occurs " + entry.getValue() + " times in the HTML.");
    }
    return;
  }
}
